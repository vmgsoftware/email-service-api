FROM mcr.microsoft.com/dotnet/core/sdk:2.2 AS build-env
WORKDIR ./EmailServiceApi

# Copy csproj and restore as distinct layers
COPY ./EmailServiceApi/*.csproj ./
RUN dotnet restore

# Copy everything else and build
COPY ./EmailServiceApi ./
RUN dotnet publish -c Release

ENV ASPNETCORE_URLS="https://+;http://+" 
ENV ASPNETCORE_HTTPS_PORT=5000
ENV ASPNETCORE_ENVIRONMENT Production

# Build runtime image
FROM mcr.microsoft.com/dotnet/core/aspnet:2.2
WORKDIR .
COPY --from=build-env /EmailServiceApi/bin/Release/netcoreapp2.2/publish .
ENTRYPOINT ["dotnet", "EmailServiceApi.dll"]