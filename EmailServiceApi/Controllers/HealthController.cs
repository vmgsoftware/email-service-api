using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace VMGWorkshopAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class HealthController : ControllerBase
    {
        private readonly ILogger _logger;

        public HealthController(ILogger<HealthController> logger)
        {
            _logger = logger;
        }
        
        // GET: api/health
        [HttpGet]
        public IActionResult Get()
        {
            _logger.LogDebug("Received GET request, no parameters specified.");
            return new ObjectResult(new { serverdate = DateTime.Now });
        }

        // GET: api/health/1
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            _logger.LogDebug("Received GET request with params", id);
            return new OkObjectResult(new { message = $"You supplied: {id}" });
        }

        // POST: api/health
        [HttpPost]
        public IActionResult Post([FromBody] string value)
        {
            _logger.LogDebug("Received POST request.", value);
            return new OkObjectResult(new { message = $"You supplied: {value}" });
        }
        
        // POST: api/health/slack
        [HttpPost("Slack")]
        public IActionResult PostSlackError([FromBody] string value)
        {
            _logger.LogError($"Testing Slack error logging with Serilog service. Value supplied was: {value}");
            return new OkObjectResult(new { message = $"You successfully logged an error with Serilog with the value: {value}" });
        }

        // PUT: api/health/1
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] string value)
        {
            _logger.LogDebug("Received PUT request.", id, value);
            return new OkObjectResult(new { message = $"You supplied a parameter of '{id}' and body '{value}'" });
        }

        // DELETE: api/health/1
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _logger.LogDebug("Received DELETE request.", id);
            return new OkObjectResult(new { message = $"You 'deleted' parameter '{id}'" });
        }

        // OPTIONS: api/health
        [HttpOptions]
        public IActionResult Options()
        {
            _logger.LogDebug("Received OPTIONS request.");
            return new OkObjectResult(new { message = "You sent a OPTIONS request." });
        }
    }
}