using System;
using System.Threading.Tasks;
using EmailServiceApi.Models;
using EmailServiceApi.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Serilog.Sinks.Slack;

namespace EmailServiceApi.Controllers
{
    [ApiController]
    [Route("/api")]
    public class EmailController : ControllerBase
    {
        private readonly IEmailService _emailService;
        private readonly ILogger<EmailController> _logger;

        public EmailController(IEmailService emailService, ILogger<EmailController> logger)
        {
            _emailService = emailService;
            _logger = logger;
        }

        [HttpPost]
        [Route("sendEmail", Name = "SendEmailAsync")]
        public IActionResult SendEmailAsync(NotifcationWrapper<SimpleMailMessageWithAttachments> notification)
        {
            try
            {
                _logger.LogInformation("Initiating Send Email...");
                _emailService.SendEmail(notification.Notification);
                _logger.LogInformation("Send Email Complete...");
                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Email failed to send: {ex.Message}");
                return StatusCode(500, new
                {
                    StatusCode = 500,
                    Status = "InternalServerError",
                    Message = $"Failed sending email because the following error occurred: {ex.Message}"
                });
            }

        }
    }
}