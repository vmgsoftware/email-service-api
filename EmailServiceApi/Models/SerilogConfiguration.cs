using Serilog.Sinks.Graylog.Core.Transport;
using Serilog.Sinks.Graylog.Extended;

namespace EmailServiceApi.Models
{
    public class SerilogConfiguration
    {
        public string AppName { get; set; }
        public string GlobalLogEventLevel { get; set; }
        public ConsoleConfiguration Console { get; set; }
        public SlackConfiguration Slack { get; set; }
        
        public GrayLogConfiguration GrayLog { get; set; }

        public SerilogConfiguration()
        {
            GlobalLogEventLevel = "Debug";
            Console = new ConsoleConfiguration();
            Slack = new SlackConfiguration();
            GrayLog = new GrayLogConfiguration();
        }
    }
    
    public class ConsoleConfiguration
    {
        public string MinimumLogEventLevel { get; set; }

        public ConsoleConfiguration()
        {
            MinimumLogEventLevel = "Debug";
        }
    }

    public class SlackConfiguration
    {
        public string WebhookUri { get; set; }
        public string ChannelName { get; set; }
        public string MinimumLogEventLevel { get; set; }
        
        public SlackConfiguration()
        {
            MinimumLogEventLevel = "Error";
        }
    }
    public class GrayLogConfiguration
    {
        public string HostNameOrAddress { get; set; }
        public int Port { get; set; }
        public TransportType TransportType { get; set; }
        public string MinimumLogEventLevel { get; set; }
        public string Facility { get; set; }
        
        public GrayLogConfiguration()
        {
            MinimumLogEventLevel = "Error";
        }
    }
}