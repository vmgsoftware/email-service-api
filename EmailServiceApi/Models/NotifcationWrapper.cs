namespace EmailServiceApi.Models
{
    public class NotifcationWrapper<TMailType> where TMailType : class
    {
        public TMailType Notification { get; set; }
    }
}