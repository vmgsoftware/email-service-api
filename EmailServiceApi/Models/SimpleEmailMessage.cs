using System;
using System.Collections.Generic;
using System.Net.Mail;

namespace EmailServiceApi.Models
{
    public class SimpleEmailMessage
    {
        public string ToList { get; set; }
        public string CcList { get; set; }
        public string BccList { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public string MessageSubject { get; set; }
        public string ToName { get; set; }
        public string FromName { get; set; }
        public string FromEmail { get; set; }

        public bool IsBodyHtml { get; set; }

        public SimpleEmailMessage()
        {
            IsBodyHtml = true;
        }
        
        
    }
}