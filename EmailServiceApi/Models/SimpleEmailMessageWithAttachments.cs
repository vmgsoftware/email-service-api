using System.Collections.Generic;

namespace EmailServiceApi.Models
{
    public class SimpleMailMessageWithAttachments : SimpleEmailMessage
    {
        public Dictionary<string, object> Attachments { get; set; }
        
        public override string ToString()
        {
            return
                $"[ToList:{ToList}] [CcList:{CcList}] [BccList:{BccList}] [Subject:{Subject}] [Message:{Message}] [MessageSubject:{MessageSubject}] [ToName:{ToName}] [FromName:{FromName}] [FromEmail:{FromEmail}] [Attachment count:{Attachments.Count}]";
        }
    }
}