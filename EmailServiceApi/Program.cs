﻿using System;
using EmailServiceApi.Extensions;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Serilog;
using EmailServiceApi.Helpers;
using ConfigurationProvider = EmailServiceApi.Providers.ConfigurationProvider;

namespace EmailServiceApi
{
    public class Program
    {
        private static IConfiguration Configuration { get; } = new ConfigurationBuilder()
            .BuildConfiguration();
        
        public static void Main(string[] args)
        {
            try
            {
                Console.Title = "VMG Email Service API";
                
                ConfigurationProvider.LoadConfiguration(Configuration);
                LoggingHelper.CreateSerilogLogger(Configuration, ConfigurationProvider.ApiConfiguration.Serilog);

                Log.Information("Building Web Host...");
                IWebHost host = CreateWebHostBuilder(args).Build();
                Log.Information("Web Host successfully built");
                
                Log.Information("Running Web Host...");
                host.Run();
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Host terminated unexpectedly!");
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseConfiguration(Configuration)
                .UseSerilog()
                .UseStartup<Startup>();
    }
}