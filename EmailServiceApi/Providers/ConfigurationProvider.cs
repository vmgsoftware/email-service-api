using EmailServiceApi.Models;
using Microsoft.Extensions.Configuration;

namespace EmailServiceApi.Providers
{
    public class ApiConfiguration
    {
        public MailConfig MailConfig { get; set; }
        public SerilogConfiguration Serilog { get; set; }
    }

    public static class ConfigurationProvider
    {
        public static ApiConfiguration ApiConfiguration { get; set; }

        public static void LoadConfiguration(IConfiguration configuration)
        {
            ApiConfiguration = new ApiConfiguration();
            
            // Loads the SMTP mail specific configuration from the appsettings.json file.
            IConfigurationSection apiConfigSection = configuration.GetSection("EmailConfig");
            ApiConfiguration.MailConfig = apiConfigSection.Get<MailConfig>();
            
            // Loads the Serilog configuration from the appsettings.json file.
            IConfigurationSection serilogConfigSection = configuration.GetSection("SerilogConfig");
            ApiConfiguration.Serilog = serilogConfigSection.Get<SerilogConfiguration>();
        }
    }
}