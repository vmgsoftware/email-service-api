using System.IO;
using System.Reflection;

namespace EmailServiceApi.Helpers
{
    public static class AssemblyHelpers
    {
        public static string GetApplicationRoot()
        {
            return Path.GetDirectoryName(Assembly.GetEntryAssembly()?.Location);
        }
    }
}