using System;
using System.Collections.Generic;
using System.Linq;

namespace EmailServiceApi.Helpers
{
    public static class EnumerationHelpers
    {
        /// <summary>
        /// This function will retrieve a List of Enum names based on the Type specified.
        /// </summary>
        /// <typeparam name="TEnumType">The Type of the Enum object to use.</typeparam>
        /// <returns>Returns a List of all Enum names.</returns>
        public static List<string> GetAllEnumNames<TEnumType>()
        {
            return Enum.GetNames(typeof(TEnumType)).ToList();
        }

        /// <summary>
        /// This function will retrieve a single Enum name from the Enum Type specified.
        /// </summary>
        /// <typeparam name="TEnumType">The Type of the Enum object to use.</typeparam>
        /// <param name="value">The actual value of the Enum that you are looking for.</param>
        /// <returns>Returns a string value of the Enum name that was found.</returns>
        public static string GetEnumName<TEnumType>(TEnumType value)
        {
            return Enum.GetName(typeof(TEnumType), value);
        }

        /// <summary>
        /// This will try and convert the string name that was specified to a Enum value and then return whether it was successful or not.
        /// </summary>
        /// <typeparam name="TEnumType">The Type of the Enum object to use.</typeparam>
        /// <param name="fieldName">The string value to use when looking for an equivalent Enum value.</param>
        /// <param name="value">The output Enum that will contain the actual Enum value for the matched string.</param>
        /// <returns>Returns either a True or False depending on if the parsing has failed or not.</returns>
        public static bool TryParseEnum<TEnumType>(string fieldName, out TEnumType value) where TEnumType : struct
        {
            return Enum.TryParse(fieldName, true, out value);
        }
    }
}