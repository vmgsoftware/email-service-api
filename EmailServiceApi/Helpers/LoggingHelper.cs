using System;
using System.Diagnostics;
using EmailServiceApi.Models;
using Microsoft.Extensions.Configuration;
using Serilog;
using Serilog.Events;
using Serilog.Sinks.Graylog;
using Serilog.Sinks.Slack;
using Serilog.Sinks.SystemConsole.Themes;

namespace EmailServiceApi.Helpers
{
    public static class LoggingHelper
    {
        public static void CreateSerilogLogger(IConfiguration configuration, SerilogConfiguration serilogConfiguration)
        {
            string runtimeEnvironment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            LogEventLevel logEventLevel = runtimeEnvironment?.ToLower() == "production"
                ? LogEventLevel.Warning
                : LogEventLevel.Debug;
            
            EnumerationHelpers.TryParseEnum(serilogConfiguration.GlobalLogEventLevel, out LogEventLevel globalLogEventLevel);
            EnumerationHelpers.TryParseEnum(serilogConfiguration.Console.MinimumLogEventLevel, out LogEventLevel consoleLogEventLevel);

            LoggerConfiguration loggerConfiguration = new LoggerConfiguration()
                .ReadFrom.Configuration(configuration)
                .MinimumLevel.Verbose()
                .MinimumLevel.Override("Microsoft", globalLogEventLevel)
                .MinimumLevel.Override("System", globalLogEventLevel)
                .MinimumLevel.Override("Microsoft.AspNetCore.Authentication", globalLogEventLevel)
                .Enrich.FromLogContext()
                .WriteTo.Console(restrictedToMinimumLevel: consoleLogEventLevel, 
                    outputTemplate: "[{Timestamp:HH:mm:ss} {Level}] {SourceContext}{NewLine}{Message:lj}{NewLine}{Exception}{NewLine}",
                    theme: AnsiConsoleTheme.Code);

            loggerConfiguration.WriteToFileOnDisk(serilogConfiguration, logEventLevel);

            if (runtimeEnvironment?.ToLower() == "production")
                loggerConfiguration.WriteToSlackChannel(serilogConfiguration);
            
            if (runtimeEnvironment?.ToLower() == "production")
                loggerConfiguration.WriteToGrayLog(serilogConfiguration);

            Log.Logger = loggerConfiguration.CreateLogger();
            Serilog.Debugging.SelfLog.Enable(msg => Debug.WriteLine(msg));
        }
        
        private static void WriteToFileOnDisk(this LoggerConfiguration loggerConfiguration, SerilogConfiguration serilogConfiguration, LogEventLevel logEventLevel)
        {
            string logFilename = $@"Logs\{serilogConfiguration.AppName}_.log";
            const long fileSizeLimitBytes = 104857600; // 100MB File Size Limit
            
            loggerConfiguration.WriteTo.Async(configure => configure.File(logFilename,
                fileSizeLimitBytes: fileSizeLimitBytes,
                rollOnFileSizeLimit: true, // Create new log file when size limit is hit
                flushToDiskInterval: TimeSpan.FromMinutes(5),
                rollingInterval: RollingInterval.Day,
                retainedFileCountLimit: 7, // Only keep the last 7 days logs
                buffered: true,
                restrictedToMinimumLevel: logEventLevel));
        }

        private static void WriteToSlackChannel(this LoggerConfiguration loggerConfiguration, SerilogConfiguration serilogConfiguration)
        {
            EnumerationHelpers.TryParseEnum(serilogConfiguration.Slack.MinimumLogEventLevel, out LogEventLevel logEventLevel);
            
            loggerConfiguration.WriteTo.Async(configure => configure.Slack(new SlackSinkOptions
            {
                WebHookUrl = serilogConfiguration.Slack.WebhookUri,
                CustomChannel = serilogConfiguration.Slack.ChannelName,
                BatchSizeLimit = 3,
                CustomIcon = ":radioactive_sign:",
                CustomUserName = $"{serilogConfiguration.AppName}Bot",
                Period = TimeSpan.FromSeconds(10),
                ShowDefaultAttachments = true,
                ShowExceptionAttachments = true,
                MinimumLogEventLevel = logEventLevel
            }));
        }
        private static void WriteToGrayLog(this LoggerConfiguration loggerConfiguration, SerilogConfiguration serilogConfiguration)
        {
            EnumerationHelpers.TryParseEnum(serilogConfiguration.GrayLog.MinimumLogEventLevel, out LogEventLevel logEventLevel);

            loggerConfiguration.WriteTo.Graylog(new GraylogSinkOptions
            {
                HostnameOrAddress = serilogConfiguration.GrayLog.HostNameOrAddress,
                Port = serilogConfiguration.GrayLog.Port,
                TransportType = serilogConfiguration.GrayLog.TransportType,
                MinimumLogEventLevel = logEventLevel,
                Facility = serilogConfiguration.GrayLog.Facility
            });
        }
    }
}