using System;
using EmailServiceApi.Helpers;
using Microsoft.Extensions.Configuration;

namespace EmailServiceApi.Extensions
{
    public static class ConfigurationBuilderExtensions
    {
        public static IConfiguration BuildConfiguration(this ConfigurationBuilder configurationBuilder)
        {
            return BuildConfiguration(configurationBuilder, AssemblyHelpers.GetApplicationRoot());
        }
        
        public static IConfiguration BuildConfiguration(this ConfigurationBuilder configurationBuilder, string basePath)
        {
            return configurationBuilder
                .SetBasePath(basePath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "Production"}.json", optional: true, reloadOnChange: true)
                .Build();
        }
    }
}