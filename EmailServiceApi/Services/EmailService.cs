using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading.Tasks;
using EmailServiceApi.Models;
using EmailServiceApi.Providers;
using System.Text;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace EmailServiceApi.Services
{
    public class EmailService : IEmailService
    {
        private static ILogger<EmailService> _logger;
        private static MailConfig _config;

        public EmailService(ILogger<EmailService> logger)
        {
            _logger = logger;
            _config = ConfigurationProvider.ApiConfiguration.MailConfig;
        }

        public void SendEmail(SimpleMailMessageWithAttachments message)
        {
            try
            {
                var mailMessage = CreateMailMessage(message);
                mailMessage.Body = ConstructSimpleEmailBody(message);

                foreach (var kvp in message.Attachments)
                {
                    var attachments = GetAttachment(kvp);
                    if (attachments != null)
                        mailMessage.Attachments.Add(attachments);
                }

                var smtpClient = CreateSmtpClient();
                smtpClient.Send(mailMessage);
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occured while trying to send email: {ex.Message}", ex);
            }
        }



        private Attachment GetAttachment(KeyValuePair<string, object> attachment)
        {
            try
            {
                if (attachment.Value.ToString().StartsWith("unsafe")) return null; // don't add unsafe data
                var typeAndData = attachment.Value.ToString().Split(',');
                // split 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAgAAAQABAAD'

                if (typeAndData.Length < 2)
                {
                    byte[] data = GetBytes(typeAndData[0]);
                    // Unknown data type, assume it is only data

                    var mailAttachment = new Attachment(new MemoryStream(data), attachment.Key);
                    return mailAttachment;

                }
                else
                {
                    var typeData = typeAndData[0].Split(':'); // split data:image/jpeg;base64
                    var base64Encoded = false;
                    var type = "image/jpeg";
                    if (typeData.Length > 1)
                        type = typeData[1];
                    if (type.Contains("base64"))
                        base64Encoded = true;
                    if (type.Contains(";"))
                        type = type.Split(';')[0]; // split image/jpeg;base64
                    byte[] data = base64Encoded
                        ? Convert.FromBase64String(typeAndData[1])
                        : GetBytes(typeAndData[1]);

                    return CreateAttachment(data, attachment.Key, type);
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occured while trying to Create SMTP Client: {ex.Message}", ex);
            }
            
        }

        private static Attachment CreateAttachment(byte[] data, string fileName, string type)
        {
            try
            {
                var attachmentStream = new Attachment(new MemoryStream(data), fileName, type)
                {
                    TransferEncoding = TransferEncoding.Base64
                };

                var disposition = attachmentStream.ContentDisposition;
                disposition.CreationDate = DateTime.Now;
                disposition.ModificationDate = DateTime.Now;
                disposition.ReadDate = DateTime.Now;
                disposition.FileName = fileName;
                disposition.Size = data.Length;
                disposition.DispositionType = DispositionTypeNames.Attachment;


                return attachmentStream;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occured while trying to Create Attachment: {ex.Message}", ex);
            }
        }

        private static SmtpClient CreateSmtpClient()
        {
            try
            {
                var smtpClient = new SmtpClient
                {
                    Host = _config.Host,
                    Port = _config.Port,
                    EnableSsl = _config.EnableSsl,
                    UseDefaultCredentials = _config.UseDefaultCredentials,
                    Credentials = new NetworkCredential(_config.User, _config.Password)
                };
            
                return smtpClient;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occured while trying to Create SMTP Client: {ex.Message}", ex);
            }

        }
        
        private static MailMessage CreateMailMessage(SimpleEmailMessage simpleMailMessage)
        {
            try
            {
                var message = new MailMessage();
                var fromAddress =
                    new MailAddress(_config.FromEmail ?? "webmaster@vmgsoftware.co.za");
                message.From = fromAddress;
                message.To.Add(simpleMailMessage.ToList);
                if (!string.IsNullOrEmpty(simpleMailMessage.CcList))
                    message.CC.Add(simpleMailMessage.CcList);
                if (!string.IsNullOrEmpty(simpleMailMessage.BccList))
                    message.Bcc.Add(simpleMailMessage.BccList);
                message.Subject = simpleMailMessage.Subject;
                message.IsBodyHtml = simpleMailMessage.IsBodyHtml;
                return message;
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occured while trying to Create Mail Message: {ex.Message}", ex);
            }

        }

        private static string ConstructSimpleEmailBody(SimpleEmailMessage body)
        {
            try
            {
                var formattedMessage = body.Message.Replace("\n", "<br />");

                var contextData = new
                {
                    To = body.ToName,
                    From = body.FromName,
                    Email = body.FromEmail,
                    body.Subject,
                    Message = formattedMessage
                };

                var templateRenderService = new TemplateRenderService(TemplateType.EmailWithAttachments);
                var template = templateRenderService.RenderTemplate("Email Enquiry Received", null, contextData);
                return template;
            }
            catch (Exception ex)
            {
                throw new Exception(
                    $"An error occured while trying to Construct Simple Email Body Mail Message: {ex.Message}", ex);
            }
        }

        private static byte[] GetBytes(string str)
        {
            var bytes = new byte[str.Length * sizeof(char)];
            Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }
    }
}