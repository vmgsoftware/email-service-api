using System;
using System.IO;
using HandlebarsDotNet;

namespace EmailServiceApi.Services
{
    public enum TemplateType
    {
        EmailWithAttachments,
    }
    public class TemplateRenderService
    {
        private Func<object, string> Template { get; }
        private readonly TemplateType _templateType;
        
        public TemplateRenderService(TemplateType templateType)
        {
            try
            {
                _templateType = templateType;
                
                RegisterHelpers();
                RegisterTemplatePartials();
                Template = Handlebars.Compile(File.ReadAllText("Views/Layout/layout.hbs"));
                
            }
            catch (Exception ex)
            {
                throw new Exception($"Could not compile the {GetEnumName(templateType)} template.", ex);
            }
        }
        
        public string RenderTemplate(string title, object layoutContext, object contextData)
        {
            try
            {
                if (Template == null)
                    throw new Exception("Template has not been initialised. Please initialise this service first.");
                
                var data = new {
                    title,
                    layoutContext,
                    contextData
                };
                
                return Template(data);
            }
            catch (Exception ex)
            {
                throw new Exception($"Failed to render the {GetEnumName(_templateType)} template.", ex);
            }
        }
        
        private void RegisterHelpers()
        {
            void StringEqualityBlockHelper(TextWriter output, HelperOptions options, dynamic context, object[] arguments)
            {
                if (arguments == null || arguments.Length != 2) throw new HandlebarsException("{{ifEquals}} helper must have exactly two argument");

                var arg1 = arguments[0] as string;
                var arg2 = arguments[1] as string;
                if (string.Equals(arg1, arg2, StringComparison.CurrentCultureIgnoreCase))
                    options.Template(output, null);
                else
                    options.Inverse(output, null);
            }

            Handlebars.RegisterHelper("ifEquals", StringEqualityBlockHelper);
        }
        
        private void RegisterTemplatePartials()
        {
            Handlebars.RegisterTemplate("header", "");
            Handlebars.RegisterTemplate("footer", "");
            
            string bodyTemplate = GetBodyContent();
            Handlebars.RegisterTemplate("body", bodyTemplate);
        }
        
        private string GetBodyContent()
        {
            switch (_templateType)
            {
                case TemplateType.EmailWithAttachments:
                    return File.ReadAllText("Views/email.hbs");
                default:
                    throw new ArgumentOutOfRangeException(nameof(_templateType), _templateType, null);
            }
        }
        
        private static string GetEnumName<TEnumType>(TEnumType value)
        {
            return Enum.GetName(typeof(TEnumType), value);
        }
        
    }
}