using System.Threading.Tasks;
using EmailServiceApi.Models;

namespace EmailServiceApi.Services
{
    public interface IEmailService
    {
        void SendEmail(SimpleMailMessageWithAttachments message);
    }
}